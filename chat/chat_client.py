from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import sys

def receber():
  # Recebe as mensagens em um loop para poder receber a qualquer momento

  while True:
    try:
      mensagem = client_socket.recv(BUFSIZ).decode("utf8")
      print("\n" + mensagem)

    # O cliente provavelmente saiu do chat
    except OSError:
      break


def enviar():
  # Envia as mensagens para o server
  while True:

    mensagem = input()

    # Enviar a mensagem ao server
    client_socket.send(bytes(mensagem, "utf8"))

    # Caso seja um comando de saída
    if mensagem == "/sair":
      client_socket.close()
      break
      
HOST = input('Digite o endereço: ')

PORT = input('Digite a porta: ')
if not PORT:
  PORT = 8000
else:
  PORT = int(PORT)

PROTOCOLO = input('Digite o protocolo: ').lower()

if PROTOCOLO is None:
    PROTOCOLO = 'tcp'
elif PROTOCOLO == 'udp':
  print('Ainda nao implementado.')
  exit()

BUFSIZ = 1024
ADDR = (HOST, PORT)

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(ADDR)

recieve_thread = Thread(target=receber)
recieve_thread.start()

send_thread = Thread(target=enviar)
send_thread.start()
