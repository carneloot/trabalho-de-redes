from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread

def receber_conexoes() :
  # Receberá todas as conexões e colocará no chat
  # Ficará em loop para receber conexões a qualquer momento

  while True:
    # Recebe o par de socket (cliente, endereço_do_cliente) do método accept
    client, client_adress = SERVER.accept()   
    
    # Avisar no server-side que o cliente se conectou
    print("%s:%s conectou-se" % client_adress)

    # Pedir nome do usuário
    client.send(bytes("Bem vindo ao chat! Digite seu nome: ", "utf8"))

    # Coloca o usuário no dicionário de clientes
    adresses[client] = client_adress

    # Inicia um thread com o novo usuário
    Thread(target = tratar_cliente, args = (client, )).start()        

# Recebe como argumento o primeiro item do socket da função receber conexões
def tratar_cliente(client):

  # Recebe o nome do usuário
  nome = client.recv(BUFSIZ).decode("utf8")

  # Da boas vindas ao usuário
  boas_vindas = 'Bem vindo %s, quando quiser sair digite /sair.' % nome
  client.send(bytes(boas_vindas, "utf8"))

  # Avisar a todos que há um novo membro no chat
  mensagem_aviso = '%s entrou no chat!' % nome
  transmitir(bytes(mensagem_aviso, "utf8"), "[Server]: ", "server")

  # Adiciona o usuário ao dicionário de clientes
  clients[client] = nome

  # Loop do cliente, será usado para receber as mensagens e transmiti-las
  while True:
    mensagem = client.recv(BUFSIZ)

    # Se a mensagem não for para sair do chat:
    if mensagem != bytes("/sair", "utf8"):
      transmitir(mensagem, nome+": ", client)
    
    else:
      # Mandar para o client-side a saida
      client.send(bytes("[Server]: Saindo...\n", "utf8"))

      # Avisar a todos que o usuário saiu
      transmitir(bytes("[Server]: %s saiu." % nome, "utf8", "server"))
      print("%s desconectou-se" % nome)

      # Cortar conexão com cliente
      client.close()

      # Tirar o usuário do dicionário de clientes
      del clients[client]

      break

# Envia mensagem a todos
def transmitir(mensagem, prefixo="", sender=""):
  for sock in clients:
    if sock != sender:
      sock.send(bytes(prefixo, "utf8") + mensagem )

# Dicionários
clients  = {} # Dicionário de clientes
adresses = {} # Dicionário de endereços

# Constantes
HOST = ''           # IP do server
PORT = 8000         # Porta do server
BUFSIZ = 1024       # Tamanho das mensagens transmitidas
ADDR = (HOST, PORT) # Define o par que será usado em bind

# Definindo socket padão
SERVER = socket(AF_INET, SOCK_STREAM)

# ligando o server
SERVER.bind(ADDR)

if __name__ == "__main__":
  SERVER.listen(5)
  print("Aguardando conexao...")
  ACCEPT_THREAD = Thread(target=receber_conexoes)
  ACCEPT_THREAD.start()
  ACCEPT_THREAD.join()
  SERVER.close()