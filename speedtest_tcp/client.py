from report import Report
from receber import Receiver
from enviar import Sender
import argparse
from time import time, sleep
import socket as sk
import json

START_TIME = 0
TEST_TIME = 20
RELATORIO_UP = Report()

class Client:

    def __init__(self):
        self.args_sender = argparse.Namespace(
            host = '',
            filename = 'teste.pdf',
            port = 8080,
            buffersize = 2000,
            window = 6,
            connecttimeout = 20,
            sendtimeout = 20,
        )
        self.host = ''
        self.args_receiver = None
        self.tcpSocket = None
        self.conn = None

        self.connectTimeout = self.args_sender.connecttimeout
        self.port = self.args_sender.port

    def run_test(self):
        
        # Testar ping da conexão por tcp
        self.test_ping()
        sleep(2)

        # Testar velocidade download
        print('----Iniciando download----')
        receiver = Receiver(self.args_receiver)

        receiver.run()

        # Receber resultado download
        sleep(7)
        print('----Iniciando Recebimento Resultado----')
        self.connectTcp()
        resultado_down = self.receber_resultado()

        # Fechar conexão tcp
        self.tcpSocket.close()
        self.tcpSocket = None
        if self.conn:
            self.conn.close()
            self.conn = None

        sleep(5)

        # Testar velocidade upload
        print('----Iniciando Upload----')
        sender = Sender(self.args_sender)
        sender.addEvent('start_ftp', start_timer)
        sender.addEvent('after_send', test_check_up)

        sender.run()
        print('----Fim Upload----')
        
        # Escrever resultado
        print('---- Resultados ----')
        print(f'Ping = {self.ping:.2f} ms')
        self.tratar_resultado_down(resultado_down)
        self.tratar_resultado_up()


    def test_ping(self):
        print('---- Testando Ping ----')
        self.connectTcp()
        self.tcpSocket.setblocking(True)
        package = bytearray(56)
        total_time = 0

        for count in range(10):
            start = time()
            self.tcpSocket.send(package)
            data = self.tcpSocket.recv(200)
            end = time()

            total_time += end - start
        
        # Ping em segundos
        self.ping = total_time/10
        # Ping em milisegundos
        self.ping = self.ping * 1000

        # Fechar conexão tcp
        self.tcpSocket.close()
        self.tcpSocket = None
        if self.conn:
            self.conn.close()
            self.conn = None

    def connectTcp(self):
        self.tcpSocket = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
        self.tcpSocket.settimeout(self.connectTimeout)

        self.tcpSocket.setblocking(True)

        
        try:
            self.tcpSocket.connect((self.host, 8082))
            print('Conexão estabelecida.')
            
        except sk.timeout:
            print('Tempo limite de conexão excedido.')
            exit(1)


    def receber_resultado(self):
        ans = str(self.tcpSocket.recv(1024), encoding = 'utf-8')
        resultado = json.loads(ans)
        return resultado
    
    def tratar_resultado_down(self, resultado):
        qtd_packs = resultado['qtd_packs']
        qtd_perda = resultado['qtd_perda']
        bufferSize = resultado['buffersize']
        headerSize = resultado['headersize']
        test_time = resultado['test_time']

        velocidade = (bufferSize + headerSize) * qtd_packs * 8 / test_time
        velocidade = velocidade/1000000
        velocidade = f'{velocidade:.3f}'
        pacotes_p_segundo = qtd_packs/test_time
        total_pacotes = qtd_packs + qtd_perda
        taxa_perda = 100 * qtd_perda/total_pacotes

        print('---- Resultado Download ----')
        print('Velocidade: ', velocidade, 'Mb/s')
        print('Pacotes/s: ', pacotes_p_segundo)
        print('Taxa de perda: ', taxa_perda, '%')
        print('Tamanho do Header: ', headerSize, 'bytes')
        print('Tamanho do payload: ', bufferSize, 'bytes')

    def tratar_resultado_up(self):
        global RELATORIO_UP

        velocidade = (RELATORIO_UP.bufferSize + RELATORIO_UP.headerSize) * RELATORIO_UP.qtd_packs * 8 / TEST_TIME
        velocidade = velocidade/1000000
        velocidade = f'{velocidade:.3f}'
        pacotes_p_segundo = RELATORIO_UP.qtd_packs/RELATORIO_UP.runtime
        total_pacotes = RELATORIO_UP.qtd_packs + RELATORIO_UP.qtd_perda
        taxa_perda = 100 * RELATORIO_UP.qtd_perda/total_pacotes

        print('---- Resultado Upload ----')
        print('Velocidade: ', velocidade, 'Mb/s')
        print('Pacotes/s: ', pacotes_p_segundo)
        print('Taxa de perda: ', taxa_perda, '%')
        print('Tamanho do Header: ', RELATORIO_UP.headerSize, 'bytes')
        print('Tamanho do payload: ', RELATORIO_UP.bufferSize, 'bytes')
        


    def setup_args_sender(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('host',
                            action='store',
                            help='IP do host para enviar o arquivo')


        args = parser.parse_args()
        self.args_sender.host = args.host

    def setup_args_receiver(self):
        self.args_receiver = argparse.Namespace(
            host = self.args_sender.host,
            port = 8081,
            # connectTimeout = self.args_sender.connecttimeout
        )
        self.host = self.args_sender.host


def test_check_up(currentPacket, bufferSize, headerSize, perdas, comando):
    global RELATORIO_UP
    global START_TIME

    # Terminar teste
    RELATORIO_UP.modified = True
    RELATORIO_UP.qtd_packs = currentPacket
    RELATORIO_UP.bufferSize = bufferSize
    RELATORIO_UP.headerSize = headerSize
    RELATORIO_UP.qtd_perda = perdas
    RELATORIO_UP.runtime = time() - START_TIME

def start_timer():
        global START_TIME
        START_TIME = time()

if __name__ == '__main__':

    client = Client()

    client.setup_args_sender()
    client.setup_args_receiver()

    client.run_test()