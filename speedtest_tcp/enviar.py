import socket as sk
import json
import argparse
from progress import progressBar
from select import select
from time import time
from os import path, urandom
from events import Events

class Sender():

    # dest, buffer_size, janela, timeout_connect_sec, timeout_send_ms):
    def __init__(self, arguments):
        self.dest = (arguments.host, arguments.port)
        self.buffer_size = arguments.buffersize
        self.janela = arguments.window
        self.timeout_connect = arguments.connecttimeout
        self.timeout_send = arguments.sendtimeout / 1000
        # self.filename = arguments.filename

        self.time_to_send = 20

        self.perdas = 0
        self.header_size = 0

        # Sei que está redundante, mas estou com pressa
        self.host = arguments.host
        self.port = arguments.port

        eventTypes = ('before_send', 'after_send', 'start_ftp', 'end_ftp')
        self.events = Events(eventTypes)

    def addEvent(self, eventName, function):
        if eventName == 'before_send':
            self.events.before_send = function
        elif eventName == 'after_send':
            self.events.after_send = function
        elif eventName == 'start_ftp':
            self.events.start_ftp = function
        elif eventName == 'end_ftp':
            self.events.end_ftp = function
        else:
            raise Exception('Wrong event')

    def run(self):

        with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as socket:
            self.socket = socket

            self.connect()

            self.enviarHeader()

            self.enviarArquivo()
        
        self.socket = None

    def connect(self):
        print('Destino: ', self.dest)
        print('Aguardando conexão...')

        self.socket.setblocking(False)
        time_start = time()

        
        self.socket.settimeout(self.timeout_connect)

        self.socket.setblocking(True)

        self.socket.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)


        try:
            self.socket.connect((self.host, self.port))
            print('Conexão estabelecida.')
            # break
        except sk.timeout as e:
            print(e)
            print('Tempo limite de conexão excedido.')
            exit(1)

        self.socket.setblocking(True)

    def enviarHeader(self):
        if not self.socket:
            raise Exception('Socket nao definido')

        header = {}

        header['buffersize'] = self.buffer_size

        payload = json.dumps(header) + '\n'

        self.socket.send(bytes(payload, encoding='utf-8'))

        self.receberAck()

    def receberAck(self):
        self.socket.setblocking(False)

        ready = select([self.socket], [], [], self.timeout_send)

        answer = ''

        if ready[0]:
            answer = self.socket.recv(self.buffer_size).decode('utf-8')
        else:
            print('Reenviando pacote...')
            self.perdas += 1

        self.socket.setblocking(True)

        # Ack ignorado por causa do TCP.
        # Habilitar quando for UDP
        return True # answer == 'ACK'

    def enviarArquivo(self):
        self.confirm = True
        self.total_enviado = 0

        self.events.start_ftp()

        self.start_time = time()

        packet = True

        while packet:
            
            self.events.before_send(self.total_enviado, self.buffer_size, self.header_size, self.perdas, None)

            # For para enviar JANELA vezes o packet
            for current_window in range(self.janela):
                packet = self.lerEnviar(packet)

                if not packet:
                    break

            self.events.after_send(self.total_enviado, self.buffer_size, self.header_size, self.perdas, None)

            self.mostrarProgresso()
        
        self.events.end_ftp()
    
    def lerEnviar(self, packet):
        if self.confirm:
            packet = bytearray(urandom(self.buffer_size))
            if time() - self.start_time >= self.time_to_send:
                packet = None

        if packet is None or packet == b'':
            return None

        self.socket.send(packet)

        self.confirm = self.receberAck()

        if self.confirm:
            self.total_enviado += 1
        
        return packet

    def mostrarProgresso(self):
        progressBar(time() - self.start_time, self.time_to_send)
