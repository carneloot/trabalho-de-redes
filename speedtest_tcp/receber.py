import socket as sk
import json
from progress import progressBar
from pprint import pprint

class Receiver():

    def __init__(self, dest):
        self.dest = (dest.host, dest.port)

    def run(self):
        with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as socket:
            self._socket = socket

            self.connect()

            self.receberHeader()

            self.receberArquivo()

            self.socket.close()
        
        self._socket = None

    def connect(self):
        print('Aguardando conexão...')

        self._socket.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)

        # Rodar como um servidor para recer o arquivo
        self._socket.bind(('', self.dest[1]))
        self._socket.listen(1)

        self.socket, _ = self._socket.accept()

        print('Conexão estabelecida!')

    def enviarAck(self):
        self.socket.send(bytes('ACK', encoding='utf-8'))

    def receberString(self):
        text = ''
        while '\n' not in text:
            text += str(self.socket.recv(4), encoding='utf-8')
        # Costumava-se retornar text[:-1], mas, por algum motivo, um 'R'
        #  estava aparecendo a mais. Então, colocamos text[:-2] para removê-lo
        while text[-1] != '}':
            text = text[:-1]
        return text

    def receberHeader(self):
        request_string = self.receberString()

        self.enviarAck()

        print('@@', request_string, '@@', sep='')

        header = json.loads(request_string)

        print('Header recebido: ')
        pprint(header)

        self.buffer_size = header['buffersize']

    def receberArquivo(self):
        self.total_recebido = 0

        self.last_packet = None

        should_continue = True

        while should_continue:
            should_continue = self.receberEscrever()
        
            self.mostrarProgresso()

        print()

    def receberEscrever(self):
        packet = self.socket.recv(self.buffer_size)

        self.enviarAck()

        if packet == b'':
            return False

        if packet != self.last_packet:
            self.total_recebido += 1

            self.last_packet = packet
        else:
            print('Duplicata. Ignorando...')

        return True
    
    def mostrarProgresso(self):
        pass
        # progressBar(self.total_recebido, self.packetnumber)