import socket as sk
from enviar import Sender
from receber import Receiver
import argparse
from report import Report
from time import time, sleep
import json

START_TIME = 0
TEST_TIME = 20
RELATORIO_DOWN = Report()

class Server:

    def __init__(self):
        self.args_sender = argparse.Namespace(
            host = '',
            filename = 'teste.pdf',
            port = 8081,
            buffersize = 2000,
            window = 6,
            connecttimeout = 20,
            sendtimeout = 20,
        )
        self.args_receiver = None
        self.tcpSocket = None
        self.connn = None

        self.timeoutConnect = self.args_sender.connecttimeout
        self.port = self.args_sender.port


    def run_test(self):
        global START_TIME

        # Testar ping da conexão por tcp
        self.test_ping()

        sleep(5)

        # Testar velocidade de download
        print('---- Inicianto deste de download. ----')

        sender = Sender(self.args_sender)
        sender.addEvent('start_ftp', start_timer)
        sender.addEvent('after_send', test_check_down)

        sender.run()

        bytesPsegundo = get_bits_p_segundo()
        mbits = bits2mbits(bytesPsegundo)
        print('---- Fim upload ----')
        print('Resultado: ', mbits , 'Mbits/s')

        # Enviar resultado
        sleep(2)
        print('---- Enviando resultado. ----')
        self.startTcpServer()

        try:
            self.conn, addr = self.tcpSocket.accept()
            self.addr = addr[0]
            print(f'Conexão estabelecida com {self.addr}')

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)
        
        self.enviar_resultado(self.conn)
        print('---- Resultado enviado ----')

        # Fechando conexão TCP
        self.conn.close()
        self.conn = None
        self.tcpSocket.close()
        self.tcpSocket = None

        sleep(2)

        # Testar velocidade de upload
        print('---- Iniciando teste Upload ----')
        # self.setup_args_receiver()
        receiver = Receiver(self.args_receiver)

        receiver.run()

        print('---- Finalizado. ----')

    def startTcpServer(self):
        self.tcpSocket = sk.socket(sk.AF_INET, sk.SOCK_STREAM)

        self.tcpSocket.setblocking(False)

        self.tcpSocket.settimeout(self.timeoutConnect)

        self.tcpSocket.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)

        try:
            print('Aguardando conexão...')

            self.tcpSocket.bind(('', 8082))
            self.tcpSocket.listen(5)

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)

    def enviar_resultado(self, conn):
        global RELATORIO_DOWN
        global TEST_TIME
        resultado = {}

        resultado['qtd_packs'] = RELATORIO_DOWN.qtd_packs
        resultado['qtd_perda'] = RELATORIO_DOWN.qtd_perda
        resultado['buffersize'] = RELATORIO_DOWN.bufferSize
        resultado['headersize'] = RELATORIO_DOWN.headerSize
        resultado['test_time'] = RELATORIO_DOWN.runtime

        payload = json.dumps(resultado)

        print('Enviando resultado ao cliente')

        conn.send(bytes(payload, encoding='utf-8'))

    def setup_args_sender(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('host',
                            action='store',
                            help='IP do host para enviar o arquivo')


        args = parser.parse_args()
        self.args_sender.host = args.host
        # self.args_sender

    def setup_args_receiver(self):
        self.args_receiver = argparse.Namespace(
            host = self.args_sender.host,
            port = 8080,
            # connectTimeout = self.args_sender.connecttimeout
        )
        self.host = self.args_sender.host
    
    def test_ping(self):
        # Enviar resultado
        print('---- Testando Ping ----')
        self.startTcpServer()

        try:
            self.conn, addr = self.tcpSocket.accept()
            self.addr = addr[0]
            print(f'Conexão estabelecida com {self.addr}')

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)
        
        self.conn.setblocking(True)
        package = bytearray(56)
        

        for count in range(10):
            data = self.conn.recv(200)
            self.conn.send(package)


        # Fechando conexão TCP
        self.conn.close()
        self.conn = None
        self.tcpSocket.close()
        self.tcpSocket = None


def start_timer():
    global START_TIME
    START_TIME = time()

def test_check_down(currentPacket, bufferSize, headerSize, perdas, comando):
    global RELATORIO_DOWN
    global TEST_TIME
    global START_TIME

    
    # Terminar teste
    RELATORIO_DOWN.modified = True
    RELATORIO_DOWN.qtd_packs = currentPacket
    RELATORIO_DOWN.bufferSize = bufferSize
    RELATORIO_DOWN.headerSize = headerSize
    RELATORIO_DOWN.qtd_perda = perdas
    RELATORIO_DOWN.runtime = time() - START_TIME

def get_bits_p_segundo():
    global RELATORIO_DOWN
    global TEST_TIME

    return (RELATORIO_DOWN.bufferSize + RELATORIO_DOWN.headerSize) * RELATORIO_DOWN.qtd_packs * 8 / TEST_TIME

def bits2mbits(nbits):
    result = nbits/1000000
    return f'{result:.3f}'

if __name__ == '__main__':

    server = Server()

    server.setup_args_sender()
    server.setup_args_receiver()

    server.run_test()