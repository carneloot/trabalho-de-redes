#!/usr/bin/env python

import socket as sk
import argparse
import json
from time import time
from events import Events


class Receiver():
    def __init__(self, argumentos):
        self.host = argumentos.host
        self.port = argumentos.port
        self.connectTimeout = argumentos.connectTimeout

        self.tcpSocket = None
        self.udpSocket = None

        # Events
        eventTypes = ('before_receive', 'after_receive', 'start_ftp', 'end_ftp')
        self.events = Events(eventTypes)

    def addEvent(self, eventName, function):
        if eventName == 'before_receive':
            self.events.before_receive = function
        elif eventName == 'after_receive':
            self.events.after_receive = function
        elif eventName == 'start_ftp':
            self.events.start_ftp = function
        elif eventName == 'end_ftp':
            self.events.end_ftp = function
        else:
            raise Exception('Wrong event')

    def run(self):
        self.connectTcp()

        self.receberHeader()

        # Finalizando tcp
        # print('Enviando comando start-ftp')
        self.enviarComando('start-ftp', self.tcpSocket)
        self.tcpSocket.close()
        self.tcpSocket = None

        self.events.start_ftp()

        self.runFtp()

        self.events.end_ftp()

        self.close()
    
    def connectTcp(self):
        self.tcpSocket = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
        self.tcpSocket.settimeout(self.connectTimeout)

        self.tcpSocket.setblocking(True)

        self.tcpSocket.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)

        print('Aguardando conexão...')

        try:
            self.tcpSocket.connect((self.host, self.port))
            print('Conexão estabelecida.')
            # break
        except sk.timeout as e:
            print(e)
            print('Tempo limite de conexão excedido.')
            exit(1)

    def enviarComando(self, comando: str, socket, udp=False):
        if udp:
            socket.sendto(bytes('<>' + comando + '<>', encoding='utf-8'), (self.host, self.port))
        else:
            socket.send(bytes('<>' + comando + '<>', encoding='utf-8'))

    def receberHeader(self):
        # Pedir header
        print('Enviando comando header')
        self.enviarComando('header', self.tcpSocket)

        # Receber header
        print('Recebendo header')
        ans = str(self.tcpSocket.recv(1024), encoding = 'utf-8')
        header = json.loads(ans)

        self.window = header['window']
        self.headersize = header['headersize']
        self.buffersize = header['buffersize']
        self.packetsize = self.headersize + self.buffersize

        print('Header recebido')

    def runFtp(self):
        print('Iniciando FTP')
        self.udpSocket = sk.socket(sk.AF_INET, sk.SOCK_DGRAM)
        self.udpSocket.bind(('', self.port))

        self.currentPacket = 0

        # Iniciando vetor de checagem
        check_vect = [False for _ in range(self.window)]

        # Iniciando o vetor de dados
        data_vect = [None for _ in range(self.window)]

        shouldRun = True

        while shouldRun:

            self.events.before_receive(
                self.currentPacket, # Pacote atual
                self.packetsize,    # Packet size
            )

            # Loop da window
            for _ in range(self.window):
                # print('recebendo pacote')
                package = self.udpSocket.recv(self.packetsize)

                if package == b'':
                    shouldRun = False
                    break

                self.receivedPacket = int(package[0:self.headersize].decode('utf-8'))

                data = package[self.headersize:]

                indice = self.receivedPacket % self.window
                # print(self.receivedPacket, indice)
                data_vect[indice] = data
                check_vect[indice] = True

            comandoString = ''

            if not shouldRun:
                break

            # Todos os pacotes recebidos com sucesso
            if self.checkIfValid(data_vect, check_vect):
                # Pedir próxima window
                comandoString = 'next-batch:' + str(self.currentPacket)

                self.enviarComando(comandoString, self.udpSocket, True)

                # Incrementar pacote atual
                self.currentPacket += self.window

            else:
                # Algo deu errado! Me manda o que acabou de mandar
                comandoString = 'next-batch:' + str(self.currentPacket - self.window)

                self.enviarComando(comandoString, self.udpSocket, True)

            self.events.after_receive(
                self.currentPacket, # Pacote atual
                self.packetsize,    # Packet size
                comandoString,      # Comando enviado
            )

            # Reiniciar vetor de checagem
            check_vect = [False for _ in range(self.window)]

        print()
        print('Fim do recebimento')

    def checkIfValid(self, data_vect, check_vect):
        if self.receivedPacket < self.currentPacket:
            return False

        # Checando se todos os pacotes foram recebidos
        # print('Checando se todos os pacotes foram recebidos')
        for _, item in enumerate(check_vect):
            if item == False:
                return False

        return True

    def close(self):

        if self.tcpSocket:
            self.tcpSocket.close()
            self.tcpSocket = None

        if self.udpSocket:
            self.udpSocket.close()
            self.udpSocket = None

START_TIME = 0

def startTimer():
    global START_TIME
    START_TIME = time()

def endTimer():
    global START_TIME
    endTime = time()

    print(f'Tempo demorado = {endTime - START_TIME} s')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('host',
                        action='store', help='Nome do host')

    parser.add_argument('--port',
                        action='store', dest='port',
                        required=False, default=8080, type=int,
                        help='Porta a ser utilizada pelo sistema. Padrao: 8080')

    parser.add_argument('--connect-timeout',
                        action='store', dest='connectTimeout',
                        required=False, default=10, type=int,
                        help='Tempo de timeout para conectar em segundos. Padrao: 10')

    arguments = parser.parse_args()

    receiver = Receiver(arguments)

    receiver.addEvent('start_ftp', startTimer)
    receiver.addEvent('end_ftp', endTimer)

    try:
        receiver.run()
    except KeyboardInterrupt:
        receiver.close()