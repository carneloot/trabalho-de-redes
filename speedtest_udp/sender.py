#!/usr/bin/env python

import socket as sk
import sys
import argparse
import math
import re
import json
from os import path, urandom
from time import time
from progress import progressBar
from events import Events
from report import Report

class Sender():

    def __init__(self, argumentos):
        self.bufferSize = argumentos.buffersize
        self.window = argumentos.window
        # self.fullFilename = argumentos.filename

        self.port = argumentos.port
        self.timeoutConnect = argumentos.connecttimeout

        # Dividido por mil para transformar em segundos
        self.commandTimeout = argumentos.commandtimeout / 1000
        self.commandMaxTries = argumentos.commandmaxtries

        self.udpSocket = None
        self.tcpSocket = None
        self.file = None
        self.conn = None

        # Metricas
        self.perdas = 0
        self.endTime = 0

        self.time_to_run = 20

        self.headerSize = 10

        # Events
        eventTypes = ('before_send', 'after_send', 'start_ftp', 'end_ftp')
        self.events = Events(eventTypes)

    def addEvent(self, eventName, function):
        if eventName == 'before_send':
            self.events.before_send = function
        elif eventName == 'after_send':
            self.events.after_send = function
        elif eventName == 'start_ftp':
            self.events.start_ftp = function
        elif eventName == 'end_ftp':
            self.events.end_ftp = function
        else:
            raise Exception('Wrong event')

    def run(self):
        self.startTcpServer()

        try:
            self.conn, addr = self.tcpSocket.accept()
            self.addr = addr[0]
            print(f'Conexão estabelecida com {self.addr}')

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)

        while True:
            # Receber comando e rodar coisas dependendo do comando
            requisicao = self.receberComando(self.conn)
            if requisicao == 'header':
                self.enviarHeader(self.conn)
            elif requisicao == 'start-ftp':
                self.conn.close()
                self.conn = None

                self.tcpSocket.close()
                self.tcpSocket = None

                self.events.start_ftp()

                self.runFtp()

                self.events.end_ftp(self.perdas)

                break
            elif requisicao is None:
                break

        print('Enviado com sucesso')

        self.close()

    def runFtp(self):
        self.udpSocket = sk.socket(sk.AF_INET, sk.SOCK_DGRAM)
        self.udpSocket.bind(('', self.port))

        self.udpSocket.setblocking(False)
        self.udpSocket.settimeout(self.timeoutConnect)

        try:
            self.udpSocket.connect((self.addr, self.port))
        except sk.timeout:
            print('Tempo de limite para conexão UDP excedido.')
            self.close()
            exit(1)

        self.currentPacket = 0
        
        self.startTime = time()
        self.shouldRun = True

        while self.shouldRun:
            response = self.sendBatch()

            if response == True: 
                self.currentPacket += self.window
                progressBar(time() - self.startTime, self.time_to_run)

    def sendBatch(self):

        # Evento antes de enviar
        # Evento depois de enviar
        self.events.before_send(
            self.currentPacket,      # Pacote atual
            self.bufferSize,         # BufferSize
            self.headerSize,         # HeaderSize
            self.perdas              # Pacotes perdidos
        )

        for itemDaJanela in range(self.window):
            header = str(itemDaJanela + self.currentPacket).zfill(self.headerSize)
            # print(f'batch[{self.currentPacket}]: {header}')

            packet = bytes(header, 'utf-8') + bytearray(urandom(self.bufferSize))
            if time() - self.startTime >= self.time_to_run:
                self.shouldRun = False
                self.udpSocket.sendto(b'', (self.addr, self.port))
                break

            self.udpSocket.sendto(packet, (self.addr, self.port))
        else:
            comando = self.receberComando(self.udpSocket)

            # Evento depois de enviar
            self.events.after_send(
                self.currentPacket,     # Pacote atual
                self.bufferSize,        # BufferSize
                self.headerSize,        # HeaderSize
                self.perdas,            # Pacotes perdidos
                comando                 # Comando recebido
            )

            if comando is None:
                return False

            finishedPacket = int(comando.split(':')[1])
            self.currentPacket = finishedPacket
            return True

    def receberComando(self, conn):
        conn.setblocking(False)
        conn.settimeout(self.commandTimeout)

        tries = 0
        while True:
            try:
                data = conn.recv(100)

                break
            except sk.timeout:
                tries += 1

                if tries == self.commandMaxTries:
                    self.perdas += 1
                    # print('Limite maximo de tentativas de comando excedido')
                    return None
                continue

        if data is None:
            return None

        string = str(data, encoding='utf-8')
        match = re.match(r'<>([\w\-:]+)<>', string)

        if match is None:
            return None

        # print(f'Recebido comando "{match.group(1)}"')
        
        return match.group(1)

    def enviarHeader(self, conn):
        header = {}

        header['window'] = self.window
        header['headersize'] = self.headerSize
        header['buffersize'] = self.bufferSize

        payload = json.dumps(header)

        print(f'Enviando header')

        conn.send(bytes(payload, encoding='utf-8'))

    def startTcpServer(self):
        self.tcpSocket = sk.socket(sk.AF_INET, sk.SOCK_STREAM)

        self.tcpSocket.setblocking(False)

        self.tcpSocket.settimeout(self.timeoutConnect)

        self.tcpSocket.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)

        try:
            print('Aguardando conexão...')

            self.tcpSocket.bind(('', self.port))
            self.tcpSocket.listen(5)

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)

    def close(self):

        # Fechando arquivo

        # Fechando conexao tcp
        if self.conn:
            self.conn.close()
            self.conn = None

        if self.tcpSocket:
            self.tcpSocket.close()
            self.tcpSocket = None

        if self.udpSocket:
            self.udpSocket.close()
            self.udpSocket = None

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('host',
                        action='store',
                        help='IP do host para enviar o arquivo')

    parser.add_argument('filename',
                        action='store',
                        help='Nome do arquivo a ser enviado')

    parser.add_argument('--port',
                        action='store',
                        required=False, default=8080, type=int,
                        help='Porta a ser utilizada pelo sistema. Padrao: 8080')

    parser.add_argument('--buffer-size',
                        action='store', dest='buffersize',
                        required=False, default=2000, type=int,
                        help='Tamanho do buffer de cada pacote. Padrao: 2000')

    parser.add_argument('--window',
                        action='store',
                        required=False, default=6, type=int,
                        help='Tamanho da janela a ser utilizada. Padrao: 6')

    parser.add_argument('--connect-timeout',
                        action='store', dest='connecttimeout',
                        required=False, default=10, type=int,
                        help='Tempo de timeout para conectar em segundos. Padrao: 10')

    parser.add_argument('--command-timeout',
                        action='store', dest='commandtimeout',
                        required=False, default=20, type=int,
                        help='Tempo de timeout para receber um comando em milissegundos. Padrao: 20')

    parser.add_argument('--command-max-tries',
                        action='store', dest='commandmaxtries',
                        required=False, default=10, type=int,
                        help='Número maximo de tentativas para receber um comando. Padrao: 10')

    arguments = parser.parse_args()

    sender = Sender(arguments)

    try:
        sender.run()
    except KeyboardInterrupt:
        sender.close()