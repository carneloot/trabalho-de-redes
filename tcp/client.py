from enviar import Sender
from receber import Receiver

BUFFER_SIZE   = 2000 # Número de bytes por bloco
WINDOW        = 2  # Número de pacotes enviados por vez

RESPONSE_TIME_MS = 20  # Tempo de espera máximo em ms
TIMEOUT          = 10  # Tempo máximo de espera para conectar em segundos

def getIpPort():

    ip = input('Digite o IP do outro cliente: [localhost] ') or 'localhost'

    if not ip:
        print('IP Obrigatório')
        exit()

    porta = int(input('Digite a porta a ser utilizada: [8080] ') or '8080')

    return (ip,porta)

if __name__ == '__main__':
    DEST = getIpPort()

    # Enviar ou receber?
    resposta = input('Deseja enviar ou receber o arquivo? [E/r] ').lower() or 'e'

    processo = None

    if resposta == 'r':
        processo = Receiver(DEST)
    else:
        processo = Sender(DEST, BUFFER_SIZE, WINDOW, TIMEOUT, RESPONSE_TIME_MS)

    processo.run()
    print()
