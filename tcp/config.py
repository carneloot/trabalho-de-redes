import configparser
import inspect, os

class Config:
    def __init__(self, nome_config:str):
        self.config_name = nome_config
        self.file_name = ''

        
        # Carregar a config
        config = configparser.ConfigParser()
        config.read('cfg.ini')

        # inspect.getfile(inspect.currentframe()) # Pega o caminho para esse arquivo a partir de quem o chamou
        # path_name = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) # Pega o caminho absoluto desse arquivo

        config['DEFAULT'] = {'ip':'localhost', 'porta':8080, 'nome_arquivo':''}


        # Se o nome for default, pegar a default
        if nome_config == 'default':
            nome_config = 'DEFAULT'


        if nome_config in config:
            # Se a config já existe
            self.ip = config[nome_config]['ip']
            self.porta = config[nome_config]['porta']
            self.file_name = config[nome_config]['nome_arquivo']

        else:
            # Se não, criar uma nova config
            self.ip = input('Digite o IP do outro cliente: [localhost] ') or 'localhost'

            if not self.ip:
                print('IP Obrigatório')
                exit()

            self.porta = int(input('Digite a porta a ser utilizada: [8080] ') or '8080')

            config[nome_config] = {'ip':self.ip, 'porta':self.porta, 'nome_arquivo':self.file_name}

        # Salvar como última sessão
        config['Ultima'] = config[nome_config]

        # Fechar arquivo de config
        # with open(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + '/cfg.ini', 'w') as configfile:

        with open('cfg.ini', 'w') as configfile:
            config.write(configfile)
    
    def get_dest(self):
        return self.ip, int(self.porta)
    
    def get_file_name(self):
        return self.file_name
    
    def set_file_name(self, file_name:str):
        self.file_name = file_name

        config = configparser.ConfigParser()
        config.read('cfg.ini')
        
        # Mudar a config
        config[self.config_name]['nome_arquivo'] = file_name
        config['Ultima']['nome_arquivo'] = file_name

        # Salvar no arquivo
        with open('cfg.ini', 'w') as configfile:
            config.write(configfile)

    



# config = configparser.ConfigParser()

# config ['DEFAULT'] = {'ip':'localhost', 
#                       'porta': '8080'}


# with open('cfg.ini', 'w') as configfile:
#     config.write(configfile)

# config = configparser.ConfigParser()

# config.read('cfg.ini')
# ip = config['DEFAULT']['ip']

# print(ip)

