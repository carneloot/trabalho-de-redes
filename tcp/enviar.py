import socket as sk
import json
from progress import progressBar
from select import select
from time import time
from os import path

class Sender():

    def __init__(self, dest, buffer_size, janela, timeout_connect_sec, timeout_send_ms):
        self.dest = dest
        self.buffer_size = buffer_size
        self.janela = janela
        self.timeout_connect = timeout_connect_sec
        self.timeout_send = timeout_send_ms / 1000

    def run(self):

        with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as socket:
            self.socket = socket

            self.connect()

            self.getFile()

            self.enviarHeader()

            self.enviarArquivo()

            self.closeFile()
        
        self.socket = None

    def getFile(self):
        filename = input('Digite o nome do arquivo a ser enviado: ')

        self.file = open(filename, 'rb')

        self.filename = path.basename(filename)
        self.filesize = path.getsize(filename)

    def closeFile(self):
        if self.file:
            self.file.close()

    def connect(self):
        print('Aguardando conexão...')

        self.socket.setblocking(False)

        time_start = time()
        while time() - time_start < self.timeout_connect:
            try:
                self.socket.connect(self.dest)
                print('Conexão estabelecida.')
                break
            except Exception:
                pass
        else:
            print('Tempo limite de conexão excedido.')
            exit(1)
            
        self.socket.setblocking(True)

    def enviarHeader(self):
        if not self.socket:
            raise Exception('Socket nao definido')

        self.packetnumber = self.filesize // self.buffer_size + 1

        header = {}

        header['filename'] = self.filename
        header['filesize'] = self.filesize
        header['packetnumber'] = self.packetnumber
        header['buffersize'] = self.buffer_size

        payload = json.dumps(header) + '\n'

        self.socket.send(bytes(payload, encoding='utf-8'))

        self.receberAck()

    def receberAck(self):
        self.socket.setblocking(False)

        ready = select([self.socket], [], [], self.timeout_send)

        answer = ''

        if ready[0]:
            answer = self.socket.recv(self.buffer_size).decode('utf-8')
        else:
            print('Reenviando pacote...')

        self.socket.setblocking(True)

        # Ack ignorado por causa do TCP.
        # Habilitar quando for UDP
        return True # answer == 'ACK'

    def enviarArquivo(self):
        self.confirm = True
        self.total_enviado = 0

        packet = True

        while packet:

            # For para enviar JANELA vezes o packet
            for _ in range(self.janela):
                packet = self.lerEnviar(packet)

                if not packet:
                    break
                
            self.mostrarProgresso()
    
    def lerEnviar(self, packet):
        if self.confirm:
            packet = self.file.read(self.buffer_size)

        if packet is None or packet == b'':
            return None

        self.socket.send(packet)

        self.confirm = self.receberAck()

        if self.confirm:
            self.total_enviado += 1
        
        return packet

    def mostrarProgresso(self):
        progressBar(self.total_enviado, self.packetnumber)
