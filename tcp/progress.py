
def progressBar(part, total, barsize = 20):
    fraction = min(part / total, 1)

    number_full = round(fraction * barsize)
    progress = ''
    for _ in range(number_full):
        progress += '#'
    for _ in range(barsize - number_full):
        progress += ' '

    percent = round(fraction * 100, 2)

    print(f'\r[ {progress} ] {percent:.2f}%', end='')