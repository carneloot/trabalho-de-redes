import threading

def progressBar(part, total, barsize = 20):
    fraction = min(part / total, 1)

    number_full = round(fraction * barsize)
    progress = ''
    for _ in range(number_full):
        progress += '#'
    for _ in range(barsize - number_full):
        progress += ' '

    percent = round(fraction * 100, 2)

    print(f'\r[ {progress} ] {percent:.2f}% {part} / {total}', end='')

class ProgressBar(threading.Thread):
    def __init__(self, size, barsize=20):
        threading.Thread.__init__(self)
        self.size = size
        self.barsize = barsize
        self.parcial = 0

    def run(self):
        while self.parcial < self.size:
            progressBar(self.parcial, self.size, self.barsize)

    def updateValue(self, value):
        self.parcial = value
