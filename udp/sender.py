#!/usr/bin/env python

import socket as sk
import sys
import argparse
import math
import re
import json
from os import path
from time import time
from progress import ProgressBar
from events import Events

class Sender():

    def __init__(self, argumentos):
        self.bufferSize = argumentos.buffersize
        self.window = argumentos.window
        self.fullFilename = argumentos.filename

        self.port = argumentos.port
        self.timeoutConnect = argumentos.connecttimeout

        # Dividido por mil para transformar em segundos
        self.commandTimeout = argumentos.commandtimeout / 1000
        self.commandMaxTries = argumentos.commandmaxtries

        self.udpSocket = None
        self.tcpSocket = None
        self.file = None
        self.conn = None

        # Metricas
        self.perdas = 0
        self.startTime = 0
        self.endTime = 0

        # Events
        eventTypes = ('before_receive', 'after_receive', 'start_ftp', 'end_ftp', 'before_send', 'after_send')
        self.events = Events(eventTypes)

    def addEvent(self, eventName, function):
        if eventName == 'before_receive':
            self.events.before_receive = function
        elif eventName == 'after_receive':
            self.events.after_receive = function
        elif eventName == 'start_ftp':
            self.events.start_ftp = function
        elif eventName == 'end_ftp':
            self.events.end_ftp = function
        else:
            raise Exception('Wrong event')

    def run(self):
        self.openFile()

        self.startTcpServer()

        try:
            self.conn, addr = self.tcpSocket.accept()
            self.addr = addr[0]
            print(f'Conexão estabelecida com {self.addr}')

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)

        while True:
            # Receber comando e rodar coisas dependendo do comando
            requisicao = self.receberComando(self.conn)
            if requisicao == 'header':
                self.enviarHeader(self.conn)
            elif requisicao == 'start-ftp':
                self.conn.close()
                self.conn = None

                self.tcpSocket.close()
                self.tcpSocket = None

                self.events.start_ftp()

                self.runFtp()

                self.events.end_ftp(self.perdas)

                break
            elif requisicao is None:
                break

        print('Enviado com sucesso')

        self.close()

    def runFtp(self):
        self.udpSocket = sk.socket(sk.AF_INET, sk.SOCK_DGRAM)
        self.udpSocket.bind(('', self.port))

        self.udpSocket.setblocking(False)
        self.udpSocket.settimeout(self.timeoutConnect)

        try:
            self.udpSocket.connect((self.addr, self.port))
        except sk.timeout:
            print('Tempo de limite para conexão UDP excedido.')
            self.close()
            exit(1)

        self.currentPacket = 0

        # Inicia a thread do ProgressBar
        self.progressBar = ProgressBar(self.packetnumber)
        self.progressBar.start()

        while self.currentPacket < self.packetnumber:
            response = self.sendBatch()

            if response == True: 
                self.currentPacket += self.window
                self.progressBar.updateValue(self.currentPacket)

    def sendBatch(self):
        self.file.seek(self.currentPacket * self.bufferSize, 0)

        # Evento antes de enviar
        self.events.before_send(
            self.currentPacket,                # Pacote atual
            self.bufferSize + self.headerSize, # Packet size
        )

        for itemDaJanela in range(self.window):
            header = str(itemDaJanela + self.currentPacket).zfill(self.headerSize)
            # print(f'batch[{self.currentPacket}]: {header}')

            packet = bytes(header, 'utf-8') + self.file.read(self.bufferSize)
            self.udpSocket.sendto(packet, (self.addr, self.port))
        else:
            comando = self.receberComando(self.udpSocket)


            if comando is None:
                # Evento depois de enviar
                self.events.after_send(
                    self.currentPacket,                # Pacote atual
                    self.bufferSize + self.headerSize, # Packet size
                    None,                              # Comando recebido
                )

                return False

            # Evento depois de enviar
            self.events.after_send(
                self.currentPacket,                # Pacote atual
                self.bufferSize + self.headerSize, # Packet size
                comando,                           # Comando recebido
            )

            finishedPacket = int(comando.split(':')[1])
            self.currentPacket = finishedPacket
            return True

    def receberComando(self, conn):
        conn.setblocking(False)
        conn.settimeout(self.commandTimeout)

        tries = 0
        while True:
            try:
                data = conn.recv(100)

                break
            except sk.timeout:
                tries += 1

                if tries == self.commandMaxTries:
                    self.perdas += 1
                    # print('Limite maximo de tentativas de comando excedido')
                    return None
                continue

        if data is None:
            return None

        string = str(data, encoding='utf-8')
        match = re.match(r'<>([\w\-:]+)<>', string)

        if match is None:
            return None

        # print(f'Recebido comando "{match.group(1)}"')
        
        return match.group(1)

    def enviarHeader(self, conn):
        header = {}

        header['window'] = self.window
        header['filename'] = self.filename
        header['filesize'] = self.filesize
        header['buffersize'] = self.bufferSize
        header['headersize'] = self.headerSize
        header['packetnumber'] = self.packetnumber

        payload = json.dumps(header)

        print(f'Enviando header')

        conn.send(bytes(payload, encoding='utf-8'))

    def openFile(self):
        self.file = open(self.fullFilename, 'rb')
        
        self.filename = path.basename(self.fullFilename)
        self.filesize = path.getsize(self.fullFilename)
        self.packetnumber = math.ceil(self.filesize / self.bufferSize)
        
        # Tamanho header é a quantidade de algarismos no packetnumber + 1
        self.headerSize = (math.floor(math.log10(self.packetnumber)) + 1) + 1

        print(f'Total do arquivo em bytes: {self.filesize}')
        print(f'Numero de packets: {self.packetnumber}')

    def startTcpServer(self):
        self.tcpSocket = sk.socket(sk.AF_INET, sk.SOCK_STREAM)

        self.tcpSocket.setblocking(False)

        self.tcpSocket.settimeout(self.timeoutConnect)

        self.tcpSocket.setsockopt(sk.SOL_SOCKET, sk.SO_REUSEADDR, 1)

        try:
            print('Aguardando conexão...')

            self.tcpSocket.bind(('', self.port))
            self.tcpSocket.listen(5)

        except sk.timeout:
            print('Tempo limite excedido')
            exit(1)

    def close(self):

        # Fechando arquivo
        if self.file:
            self.file.close()
            self.file = None

        # Fechando conexao tcp
        if self.conn:
            self.conn.close()
            self.conn = None

        if self.tcpSocket:
            self.tcpSocket.close()
            self.tcpSocket = None

        if self.udpSocket:
            self.udpSocket.close()
            self.udpSocket = None

START_TIME = 0

def startTimer():
    global START_TIME
    START_TIME = time()

def endTimer(perdas):
    global START_TIME
    endTime = time()

    print(f'Tempo decorrido: {endTime - START_TIME} s')
    print(f'Pacotes perdidos: {perdas}')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('host',
                        action='store',
                        help='IP do host para enviar o arquivo')

    parser.add_argument('filename',
                        action='store',
                        help='Nome do arquivo a ser enviado')

    parser.add_argument('--port',
                        action='store',
                        required=False, default=8080, type=int,
                        help='Porta a ser utilizada pelo sistema. Padrao: 8080')

    parser.add_argument('--buffer-size',
                        action='store', dest='buffersize',
                        required=False, default=2000, type=int,
                        help='Tamanho do buffer de cada pacote. Padrao: 2000')

    parser.add_argument('--window',
                        action='store',
                        required=False, default=6, type=int,
                        help='Tamanho da janela a ser utilizada. Padrao: 6')

    parser.add_argument('--connect-timeout',
                        action='store', dest='connecttimeout',
                        required=False, default=10, type=int,
                        help='Tempo de timeout para conectar em segundos. Padrao: 10')

    parser.add_argument('--command-timeout',
                        action='store', dest='commandtimeout',
                        required=False, default=20, type=int,
                        help='Tempo de timeout para receber um comando em milissegundos. Padrao: 20')

    parser.add_argument('--command-max-tries',
                        action='store', dest='commandmaxtries',
                        required=False, default=10, type=int,
                        help='Número maximo de tentativas para receber um comando. Padrao: 10')

    arguments = parser.parse_args()

    sender = Sender(arguments)

    sender.addEvent('start_ftp', startTimer)
    sender.addEvent('end_ftp', endTimer)

    try:
        sender.run()
    except KeyboardInterrupt:
        sender.close()