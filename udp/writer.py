import threading
from time import sleep

class Writer(threading.Thread):

    def __init__(self, filename):
        threading.Thread.__init__(self)

        self.filename = filename
        self.buffer = []
        self.still_receiving = True
        self.size = 0

    def run(self):
        with open(self.filename, 'wb') as arquivo:
            while self.still_receiving or len(self.buffer) > 0:

                if len(self.buffer) > 0:
                    packet = self.buffer.pop(0)
                    arquivo.write(packet)

                sleep(0.001)

        print(f'Arquivo {self.filename} escrito com sucesso!')

    def stopReceiving(self):
        self.still_receiving = False
    
    def addToBuffer(self, packet):
        self.buffer.append(packet)
        self.size += 1

    def addArrayToBuffer(self, array):
        self.buffer.extend(array)
        self.size += len(array)

    def getSize(self):
        return self.size